terraform {
  backend "s3" {
    bucket = "adamkhan.xyz"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
